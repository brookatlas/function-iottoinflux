**Function Description**

This is an azure function written in c#.
the functionality of this functions is simple.
everytime an azure iot hub event is triggered, the function does the following things.


1. gets the event data


2. transforms it into a valid line protocol influx-db command


3. sends the data to a influx-db instance on azure

